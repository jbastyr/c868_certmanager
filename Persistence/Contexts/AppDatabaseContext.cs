﻿using CertManager.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertManager.Persistence.Contexts
{
    public class AppDatabaseContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<AssetType> AssetTypes { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<Certificate> Certificates { get; set; }
        public DbSet<User> Users { get; set; }

        public AppDatabaseContext(DbContextOptions<AppDatabaseContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            buildTables(builder);
            seedTables(builder);
            
        }

        private void buildTables(ModelBuilder builder)
        {
            buildCustomerTable(builder);
            buildSiteTable(builder);
            buildAssetTypeTable(builder);
            buildAssetTable(builder);
            buildCertificateTable(builder);
            buildUserTable(builder);
        }

        private void seedTables(ModelBuilder builder)
        {
            seedCustomerTable(builder);
            seedSiteTable(builder);
            seedAssetTypeTable(builder);
            seedAssetTable(builder);
            seedCertificateTable(builder);
            seedUserTable(builder);
        }

        #region BuildTables
        private void buildCustomerTable(ModelBuilder builder)
        {
            builder.Entity<Customer>().ToTable("customer");
            builder.Entity<Customer>().HasKey(p => p.Id);
            builder.Entity<Customer>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Customer>().Property(p => p.Name).IsRequired().HasMaxLength(256);
            builder.Entity<Customer>().HasMany(p => p.Sites).WithOne(p => p.Customer).HasForeignKey(p => p.CustomerId);
            builder.Entity<Customer>().HasMany(p => p.Users).WithOne(p => p.Customer).HasForeignKey(p => p.CustomerId);
        }

        private void buildSiteTable(ModelBuilder builder)
        {
            builder.Entity<Site>().ToTable("site");
            builder.Entity<Site>().HasKey(p => p.Id);
            builder.Entity<Site>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Site>().Property(p => p.Name).IsRequired().HasMaxLength(256);
            builder.Entity<Site>().HasMany(p => p.Assets).WithOne(p => p.Site).HasForeignKey(p => p.SiteId);
        }

        private void buildAssetTypeTable(ModelBuilder builder)
        {
            builder.Entity<AssetType>().ToTable("asset_type");
            builder.Entity<AssetType>().HasKey(p => p.Id);
            builder.Entity<AssetType>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<AssetType>().Property(p => p.Name).IsRequired().HasMaxLength(256);

        }

        private void buildAssetTable(ModelBuilder builder)
        {
            builder.Entity<Asset>().ToTable("asset");
            builder.Entity<Asset>().HasKey(p => p.Id);
            builder.Entity<Asset>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd(); 
            builder.Entity<Asset>().Property(p => p.Name).IsRequired().HasMaxLength(256);
            builder.Entity<Asset>().Property(p => p.Data).IsRequired().HasMaxLength(1024);
            builder.Entity<Asset>().HasMany(p => p.Certificates).WithOne(p => p.Asset).HasForeignKey(p => p.AssetId);
            builder.Entity<Asset>().HasOne(p => p.AssetType);
        }

        private void buildCertificateTable(ModelBuilder builder)
        {
            builder.Entity<Certificate>().ToTable("certificate");
            builder.Entity<Certificate>().HasKey(p => p.Id);
            builder.Entity<Certificate>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Certificate>().Property(p => p.Name).IsRequired().HasMaxLength(256);
            builder.Entity<Certificate>().Property(p => p.CreatedAt).IsRequired().HasDefaultValue(DateTime.Now);
            builder.Entity<Certificate>().Property(p => p.ExpiresAt).IsRequired();
        }

        //TODO:JEREMY change the max length to match the tokens that are actually generated
        private void buildUserTable(ModelBuilder builder)
        {
            builder.Entity<User>().ToTable("user");
            builder.Entity<User>().HasKey(p => p.Id);
            builder.Entity<User>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<User>().Property(p => p.Name).IsRequired().HasMaxLength(256);
            builder.Entity<User>().Property(p => p.Password).IsRequired().HasMaxLength(1024);
            builder.Entity<User>().Property(p => p.SessionToken).HasMaxLength(1024);
            builder.Entity<User>().Property(p => p.SessionCreatedAt);
            builder.Entity<User>().Property(p => p.SessionExpiresAt);
        }
        #endregion

        #region SeedTables
        private void seedCustomerTable(ModelBuilder builder)
        {
            builder.Entity<Customer>().HasData(
                new Customer { Id = 1, Name = "Customer A" },
                new Customer { Id = 2, Name = "Customer B" },
                new Customer { Id = 3, Name = "Customer C" }
            );
        }
        private void seedSiteTable(ModelBuilder builder)
        {
            builder.Entity<Site>().HasData(
                new Site { Id = 1, CustomerId = 1, Name = "Location 1" },
                new Site { Id = 2, CustomerId = 1, Name = "Location 2" },
                new Site { Id = 3, CustomerId = 1, Name = "Location 3" },
                new Site { Id = 4, CustomerId = 2, Name = "Location 4" },
                new Site { Id = 5, CustomerId = 2, Name = "Location 5" },
                new Site { Id = 6, CustomerId = 3, Name = "Location 6" }
            );
        }
        private void seedAssetTypeTable(ModelBuilder builder)
        {
            builder.Entity<AssetType>().HasData(
                new AssetType { Id = 1, Name = "Website" },
                new AssetType { Id = 2, Name = "Application" }
            );
        }
        private void seedAssetTable(ModelBuilder builder)
        {
            builder.Entity<Asset>().HasData(
                new Asset { Id = 1, SiteId = 1, TypeId = 1, Name = "Website 1", Data = "http://notarealwebsite.fakedomain/" },
                new Asset { Id = 2, SiteId = 1, TypeId = 1, Name = "Website 2", Data = "http://notarealwebsite.fakedomain/" },
                new Asset { Id = 3, SiteId = 1, TypeId = 2, Name = "Application 1", Data = "/usr/local/app/DeployedApplication" },
                new Asset { Id = 4, SiteId = 1, TypeId = 2, Name = "Application 2", Data = "/usr/local/app/DeployedApplication" },
                new Asset { Id = 5, SiteId = 2, TypeId = 1, Name = "Website 3", Data = "http://notarealwebsite.fakedomain/" },
                new Asset { Id = 6, SiteId = 2, TypeId = 1, Name = "Website 4", Data = "http://notarealwebsite.fakedomain/" },
                new Asset { Id = 7, SiteId = 2, TypeId = 2, Name = "Application 3", Data = "/usr/local/app/DeployedApplication" },
                new Asset { Id = 8, SiteId = 3, TypeId = 1, Name = "Website 5", Data = "http://notarealwebsite.fakedomain/" },
                new Asset { Id = 9, SiteId = 3, TypeId = 2, Name = "Application 4", Data = "/usr/local/app/DeployedApplication" },
                new Asset { Id = 10, SiteId = 4, TypeId = 1, Name = "Website 6", Data = "http://notarealwebsite.fakedomain/" },
                new Asset { Id = 11, SiteId = 5, TypeId = 1, Name = "Website 7", Data = "http://notarealwebsite.fakedomain/" },
                new Asset { Id = 12, SiteId = 6, TypeId = 1, Name = "Website 8", Data = "http://notarealwebsite.fakedomain/" }
            );
        }
        private void seedCertificateTable(ModelBuilder builder)
        {
            builder.Entity<Certificate>().HasData(
                new Certificate { Id = 1, AssetId = 1, Name = "Cert 1", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 2, AssetId = 1, Name = "Cert 2", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 3, AssetId = 1, Name = "Cert 3", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MinValue },
                new Certificate { Id = 4, AssetId = 2, Name = "Cert 4", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 5, AssetId = 3, Name = "Cert 5", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 6, AssetId = 4, Name = "Cert 6", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 7, AssetId = 5, Name = "Cert 7", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 8, AssetId = 6, Name = "Cert 8", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 9, AssetId = 7, Name = "Cert 9", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 10, AssetId = 8, Name = "Cert 10", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 11, AssetId = 9, Name = "Cert 11", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 12, AssetId = 10, Name = "Cert 12", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 13, AssetId = 11, Name = "Cert 13", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue },
                new Certificate { Id = 14, AssetId = 12, Name = "Cert 14", CreatedAt = DateTime.Now, ExpiresAt = DateTime.MaxValue }
            );
        }

        // Default user passwords are all "testPassword"
        // TODO: Store only password hash, probably want salt with it etc. SCRYPT? whatever works
        private void seedUserTable(ModelBuilder builder)
        {
            builder.Entity<User>().HasData(
                new User { Id = 1, CustomerId = 1, Name = "User 1", Username = "user1", Password = "testPassword" },
                new User { Id = 2, CustomerId = 1, Name = "User 2", Username = "user2", Password = "testPassword" },
                new User { Id = 3, CustomerId = 2, Name = "User 3", Username = "user3", Password = "testPassword" },
                new User { Id = 4, CustomerId = 3, Name = "User 4", Username = "user4", Password = "testPassword" }
            );
        }
        #endregion
    }
}
