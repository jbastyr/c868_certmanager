﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CertManager.Models;

namespace CertManager.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AssetTypesController : ControllerBase
    {
        private readonly DatabaseContext _context;

        public AssetTypesController(DatabaseContext context)
        {
            _context = context;
        }

        // GET: api/AssetTypes
        [HttpGet]
        public IEnumerable<AssetType> GetAssetTypes()
        {
            return _context.AssetTypes;
        }

        // GET: api/AssetTypes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAssetType([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var assetType = await _context.AssetTypes.FindAsync(id);

            if (assetType == null)
            {
                return NotFound();
            }

            return Ok(assetType);
        }

        // PUT: api/AssetTypes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAssetType([FromRoute] long id, [FromBody] AssetType assetType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != assetType.Id)
            {
                return BadRequest();
            }

            _context.Entry(assetType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssetTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AssetTypes
        [HttpPost]
        public async Task<IActionResult> PostAssetType([FromBody] AssetType assetType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.AssetTypes.Add(assetType);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAssetType", new { id = assetType.Id }, assetType);
        }

        // DELETE: api/AssetTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAssetType([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var assetType = await _context.AssetTypes.FindAsync(id);
            if (assetType == null)
            {
                return NotFound();
            }

            _context.AssetTypes.Remove(assetType);
            await _context.SaveChangesAsync();

            return Ok(assetType);
        }

        private bool AssetTypeExists(long id)
        {
            return _context.AssetTypes.Any(e => e.Id == id);
        }
    }
}