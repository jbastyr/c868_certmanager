﻿using CertManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertManager.Services
{
    public interface ICustomerService
    {
        Task<IEnumerable<Customer>> ListAsync();
    }
}
