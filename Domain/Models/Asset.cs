﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertManager.Models
{
    public class Asset
    {
        public long Id { get; set; }
        public long SiteId { get; set; }
        public long TypeId { get; set; }
        public string Name { get; set; }
        public string Data { get; set; }

        public Site Site { get; set; }
        public AssetType AssetType { get; set; }
        public ICollection<Certificate> Certificates { get; set; }
    }
}
