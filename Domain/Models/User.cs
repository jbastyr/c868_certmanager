﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertManager.Models
{
    public class User
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        // public string PasswordSalt { get; set; } 
        public string SessionToken { get; set; }
        public DateTime SessionCreatedAt { get; set; }
        public DateTime SessionExpiresAt { get; set; }

        public Customer Customer { get; set; }
    }
}
