﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertManager.Models
{
    public class Customer
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public ICollection<Site> Sites { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
