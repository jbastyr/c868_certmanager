﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertManager.Models
{
    public class Site
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public string Name { get; set; }

        public Customer Customer { get; set; }
        public ICollection<Asset> Assets { get; set; }
    }
}
