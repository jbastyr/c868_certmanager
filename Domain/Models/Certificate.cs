﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertManager.Models
{
    public class Certificate
    {
        public long Id { get; set; }
        public long AssetId { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ExpiresAt { get; set; }

        public Asset Asset { get; set; }
    }
}
