﻿using CertManager.Persistence.Contexts;

namespace CertManager.Domain.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly AppDatabaseContext _context;

        public BaseRepository(AppDatabaseContext context)
        {
            _context = context;
        }
    }
}
