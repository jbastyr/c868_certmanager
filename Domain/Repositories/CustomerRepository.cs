﻿using CertManager.Models;
using CertManager.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CertManager.Domain.Repositories
{
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {

        public CustomerRepository(AppDatabaseContext context) : base(context)
        {

        }

        public async Task<IEnumerable<Customer>> ListAsync()
        {
            return await _context.Customers.ToListAsync();
        }
    }
}
